FROM buildpack-deps:24.04-scm
LABEL org.opencontainers.image.authors="konstantin@narkhov.pro"

ARG MAXMINDACCOUNT=0

ARG MAXMINDLICENSE=000000000000

ENV GEOIPUPD_DATADIR=/usr/share/GeoIP
ENV SECP256K1_VERSION=0.6.0
ENV GEOIPUPD_VERSION=7.1.0
ENV RAKUDO_VERSION=2025.02
ENV IMAGEMAGICK_VERSION=6.9.13-22
ENV LIBSCRYPT_VERSION=1.22

ENV PATH=/usr/bin/:/usr/share/perl6/site/bin:/usr/share/perl6/vendor/bin:/usr/share/perl6/core/bin:/usr/local/bin:$PATH

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN --mount=type=bind,target=/resources,source=resources ls -la /resources && groupadd -r raku && useradd -r -g raku raku; \
	apt-get update && apt-get install -y --no-install-recommends \
		apt-utils \
		g++ \
		gcc \
		libc6-dev \
		make \
		pkg-config \
		sudo \
		cmake \
		automake \
		libtool \
		mc \
		libencode-perl \
		libjson-maybexs-perl \
		libtest-lwp-useragent-perl \
		xz-utils \
		autoconf \
		libltdl-dev \
		libpng-dev \
		libjpeg-dev \
		libcogl-pango-dev \
		libssl-dev \
		libfreetype6-dev \
		libperl-dev \
		build-essential \
		libcurl4-openssl-dev \
		zlib1g-dev \
		vim \
		jq \
		socat \
		software-properties-common \
		bzip2 \
		uuid-dev \
		daemonize \
		libgcrypt20 \
		libgcrypt20-dev \
		libpq5 \
	&& rm -rf /var/lib/apt/lists/*; \
	\
# install libscrypt
	mkdir $HOME/libscrypt_v${LIBSCRYPT_VERSION}; \
	curl -fsSL https://github.com/technion/libscrypt/archive/refs/tags/v${LIBSCRYPT_VERSION}.tar.gz -o /libscrypt_v${LIBSCRYPT_VERSION}.tar.gz; \
	tar xzf /libscrypt_v${LIBSCRYPT_VERSION}.tar.gz -C $HOME/libscrypt_v${LIBSCRYPT_VERSION}; \
	cd $HOME/libscrypt_v${LIBSCRYPT_VERSION}/libscrypt-${LIBSCRYPT_VERSION}; \
	make && make install; \
	\
# install secp256k1
	mkdir $HOME/secp256k1_v${SECP256K1_VERSION}; \
	curl -fsSL https://github.com/bitcoin-core/secp256k1/archive/refs/tags/v${SECP256K1_VERSION}.tar.gz -o /libsecp256k1_v${SECP256K1_VERSION}.tar.gz; \
	tar xzf /libsecp256k1_v${SECP256K1_VERSION}.tar.gz -C $HOME/secp256k1_v${SECP256K1_VERSION}; \
	cd $HOME/secp256k1_v${SECP256K1_VERSION}/secp256k1-${SECP256K1_VERSION}; \
	./autogen.sh && ./configure --enable-module-recovery && make && make install; \
	\
# install solidity
	add-apt-repository ppa:ethereum/ethereum -y; \
	apt-get update && apt-get install -y solc; \
	ln -s /usr/bin/solc /usr/local/bin/solc; \
	\
# install geoipupdate
	cd /; \
	curl -fsSL https://github.com/maxmind/geoipupdate/releases/download/v${GEOIPUPD_VERSION}/geoipupdate_${GEOIPUPD_VERSION}_linux_amd64.deb -o geoipupdate.deb; \
	apt-get install -y ./geoipupdate.deb; \
	geoipupdate -V; \
	\
# update databases with geoipupdate
	mkdir -p ${GEOIPUPD_DATADIR}; \
	mkdir -p $HOME/geoipupdate/tmp && cd "$_"; \
	printf "AccountID $MAXMINDACCOUNT\nLicenseKey $MAXMINDLICENSE\nEditionIDs GeoLite2-Country GeoLite2-City\nDatabaseDirectory ${GEOIPUPD_DATADIR}\n" > $HOME/geoipupdate/tmp/GeoIP.conf; \
	geoipupdate -v -f $HOME/geoipupdate/tmp/GeoIP.conf; \
	rm -rf $HOME/geoipupdate/tmp; \
	ls -a ${GEOIPUPD_DATADIR}; \
	\
# install rakudo
	rm -rf /var/lib/apt/lists/*; \
	mkdir $HOME/rakudo; \
	tar xzf /resources/rakudo-star-$RAKUDO_VERSION.tar.gz -C $HOME/rakudo; \
	cd $HOME/rakudo/rakudo-star-$RAKUDO_VERSION; \
	bin/rstar install -p /usr; \
	export PATH=/usr/bin/:/usr/share/perl6/site/bin:/usr/share/perl6/vendor/bin:/usr/share/perl6/core/bin:$PATH; \
	\
# install imagemagick
	mkdir $HOME/imagemagick; \
	cd $HOME/imagemagick; \
	curl -fsSL https://imagemagick.org/archive/releases/ImageMagick-$IMAGEMAGICK_VERSION.tar.xz -o ImageMagick-$IMAGEMAGICK_VERSION.tar.xz; \
	tar -xpJf ImageMagick-$IMAGEMAGICK_VERSION.tar.xz -C $HOME/imagemagick; \
	cd $HOME/imagemagick/ImageMagick*; \
	./configure -with-perl; \
	make; \
	make install; \
	ldconfig /usr/local/lib; \
	\
# update zef
	zef update; \
	zef --debug install git://github.com/ugexe/zef.git; \
	zef --version; \
	\
# install Raku modules
	zef install JSON::Schema; \
	zef install Inline::Perl5; \
	zef install XML; \
	zef install GeoIP2; \
	zef install URI::Encode; \
	zef install Node::Ethereum::Keccak256::Native; \
	zef install LZW::Revolunet; \
	zef install Compress::Bzip2; \
	zef install Compress::Zlib; \
	zef install FastCGI::NativeCall; \
	zef install FastCGI::NativeCall::Async; \
	zef install Router::Right; \
	zef install https://gitlab.com/pheix/net-ethereum-perl6.git; \
	zef install NativeHelpers::Array; \
	zef install --debug --force-test https://gitlab.com/pheix/raku-bitcoin-core-secp256k1.git; \
	zef install https://github.com/2colours/html-template.git; \
	zef install https://gitlab.com/pheix/raku-crypt-libgcrypt.git; \
	zef install --debug --force-test https://github.com/pheix/perl6-magickwand.git; \
	zef install https://gitlab.com/pheix/raku-crypt-libscrypt.git; \
	zef install https://gitlab.com/pheix/raku-node-ethereum-keystore.git; \
	zef install Node::Ethereum::RLP; \
	zef install HTML::EscapeUtils; \
	zef install Base58; \
	\
# install Node::Ethereum::KZG
	git clone --recurse-submodules -j8 https://gitlab.com/pheix/raku-node-ethereum-kzg.git $HOME/git/raku-node-ethereum-kzg; \
	cd $HOME/git/raku-node-ethereum-kzg; \
	zef install --debug  .; \
# remove installation folders
	cd /; \
	rm -rf $HOME/rakudo; \
	rm -rf $HOME/imagemagick; \
	rm -rf $HOME/geoipupdate; \
	rm -rf $HOME/libscrypt_v${LIBSCRYPT_VERSION}; \
	rm -rf $HOME/secp256k1_v${SECP256K1_VERSION}; \
	apt-get purge -y make cmake gcc g++ pkg-config autoconf build-essential; \
#	dpkg -l | grep -E "\-dev\s" | grep -v "ssl" | awk '{print $2}' | xargs -d '\n' -- apt-get purge -y $1; \
	dpkg -l | grep -E "\-dev\s" | grep -v "ssl" | awk '{print $2}' | xargs -d '\n' -- apt-get purge -y; \
	apt-get clean; \
	rm -rf /var/lib/apt/lists/*; \
	rm -f /geoipupdate.deb; \
	rm -f /libscrypt_v${LIBSCRYPT_VERSION}.tar.gz; \
	rm -f /libsecp256k1_v${SECP256K1_VERSION}.tar.gz; \
	\
# test versions
	jq --version; \
	vi --version; \
	convert --version; \
	raku --version; \
	/usr/local/bin/solc --version; \
	geoipupdate -V;

CMD ["/bin/bash"]
