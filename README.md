# Rakudo docker for Pheix dCMS

This [Rakudo Star](https://gitlab.com/pheix/rakudo-star-linux) v2025.02 docker image for Pheix dCMS. This is Raku distribution that includes MoarVM virtual machine, Raku compiler, a suite of modules and language documentation.

Image contents:

1. Ethereum [Solidity](https://github.com/ethereum/solidity/releases/tag/v0.8.28) compiler (v0.8.28 from `ppa:ethereum/ethereum` repository).

2. Geoipupdate 7.1.0: https://github.com/maxmind/geoipupdate/releases/tag/v7.1.0

3. ImageMagick 6.9.13-22: https://imagemagick.org/archive/releases/

4. Optimized C library for ECDSA signatures (secp256k1) 0.6.0: https://github.com/bitcoin-core/secp256k1/releases

5. Additional Raku modules:
    * [JSON::Schema](https://github.com/croservices/json-schema)
    * [Inline::Perl5](https://github.com/niner/Inline-Perl5)
    * [XML](https://github.com/raku-community-modules/XML/)
    * [GeoIP2](https://github.com/bbkr/GeoIP2)
    * [URI::Encode](https://github.com/raku-community-modules/URI-Encode)
    * [Node::Ethereum::Keccak256::Native](https://gitlab.com/pheix/raku-node-ethereum-keccak256-native)
    * [LZW::Revolunet ](https://gitlab.com/pheix/lzw-revolunet-perl6/)
    * [Compress::Bzip2](https://github.com/Altai-man/perl6-Compress-Bzip2)
    * [Compress::Zlib](https://github.com/retupmoca/P6-Compress-Zlib)
    * [FastCGI::NativeCall](https://github.com/jonathanstowe/raku-fastcgi-nativecall)
    * [FastCGI::NativeCall::Async](https://github.com/jonathanstowe/FastCGI-NativeCall-Async/)
    * [Router::Right](https://gitlab.com/pheix/router-right-perl6)
    * [Net::Ethereum](https://gitlab.com/pheix/net-ethereum-perl6)
    * [Bitcoin::Core::Secp256k1](https://gitlab.com/pheix/raku-bitcoin-core-secp256k1)
    * [HTML::Template](https://github.com/2colours/html-template)
    * [Crypt::LibGcrypt](https://gitlab.com/pheix/raku-crypt-libgcrypt.git)
    * [MagickWand (fork)](https://github.com/pheix/perl6-magickwand)
    * [Crypt::LibScrypt:auth<zef:knarkhov>](https://gitlab.com/pheix/raku-crypt-libscrypt)
    * [Crypt::LibGcrypt](https://gitlab.com/pheix/raku-crypt-libgcrypt)
    * [Node::Ethereum::KeyStore::V3](https://gitlab.com/pheix/raku-node-ethereum-keystore)
    * [Node::Ethereum::RLP](https://gitlab.com/pheix/raku-node-ethereum-rlp)
    * [HTML::EscapeUtils](https://codeberg.org/demanuel/HTML-EscapeUtils)
    * [NativeHelpers::Array](https://github.com/jonathanstowe/NativeHelpers-Array)
    * [Base58](https://github.com/grondilu/base58-raku)
    * [Node::Ethereum::KZG](https://gitlab.com/pheix/raku-node-ethereum-kzg)

## Build

### Before Build

Rakudo Star sources are used while docker image building. Actually, [official ones](https://rakudo.org/star) have a few errors and misconfigurations. I prepared my custom Rakudo Star sources distribution — please check https://gitlab.com/pheix/rakudo-star-linux for details.

Since custom `rakudo-star-<RAKUDO_VER>.tar.gz` file (for instance `rakudo-star-2024.07.tar.gz`) is created and stored in `/resources` on your workstation, you can mount it by `RUN` statement arguments and the custom sources will be used while the building inside you builder container:

```
RUN --mount=type=bind,target=/resources,source=resources ...
```

Ref: https://gitlab.com/pheix-docker/rakudo-star/-/blob/master/Dockerfile#L16

⚠️ Custom `rakudo-star-<RAKUDO_VER>.tar.gz` in `/resources` might be dependent on source platform, where it was built: https://gitlab.com/pheix-docker/rakudo-star/-/issues/8.

#### Can I fork it? / Private runner

I use private runner on dedicated server (host) to build this docker image via GitLab. My server has a pool of pre-deployed custom Rakudo Star sources on board:

```bash
ls -la /resources

# drwxr-xr-x 2 root root     4096 Mar  3 15:32 .
# drwxr-xr-x 1 root root     4096 Mar  3 15:33 ..
# -rw-r--r-- 1 root root 18546908 Mar  3 15:32 rakudo-star-2021.04.tar.gz
# -rw-r--r-- 1 root root 27565603 Mar  3 15:32 rakudo-star-2021.12.tar.gz
# -rw-r--r-- 1 root root 33767098 Mar  3 15:32 rakudo-star-2022.02.tar.gz
# -rw-r--r-- 1 root root 27634796 Mar  3 15:32 rakudo-star-2022.03.tar.gz
# -rw-r--r-- 1 root root 14292985 Mar  3 15:32 rakudo-star-2022.04.tar.gz
# -rw-r--r-- 1 root root 27809101 Mar  3 15:32 rakudo-star-2022.07.tar.gz
# -rw-r--r-- 1 root root 28689751 Mar  3 15:32 rakudo-star-2022.12.tar.gz
# -rw-r--r-- 1 root root 28405267 Mar  3 15:32 rakudo-star-2023.02.tar.gz
```

So, if you will fork this repository and try to build the image via embedded GitLab shared runner — build will fail: https://gitlab.com/melezhik/docker-core-perl6/-/jobs/3561749212. The reason is:

```bash
mkdir ./resources && cp -rf /resources/*.tar.gz ./resources

# cp: can't stat '/resources/*.tar.gz': No such file or directory
```

Obviously embedded GitLab shared runners are executed on hosts with no Rakudo Star sources in `/resources`.

### Here we go!

You can build an image from this Dockerfile:

    $ sudo docker build -t pheix-test-image /path_to_dockerfile/

It will build docker image with the Rakudo Star v2025.02. Check out [wiki](https://gitlab.com/pheix-docker/rakudo-star/wikis/home) for detailed build instructions.

## Usage

Simply running a container with the image will launch a bash session:

    $ sudo docker run -it pheix-test-image
    root@cc715632ef40:/#

You can also provide `raku` command line switches to `docker run`:

    $ docker run -it pheix-test-image raku -e 'say "Hello!"'

In addition, you can mount a directory from the host within a container and forward the network from host:

    $ docker run -it --net=host -v $HOME/my_raku_projects/:/mount_location/ pheix-test-image /bin/bash

Then, you can run your scripts from inside the container:

    # raku /mount_location/my_raku_script.raku

## Version substitution

HOW-TO for Solidity version retrieval on Fedora: https://gitlab.com/pheix-docker/rakudo-star/-/issues/5#note_1051458421

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## Rakudo and Raku language credits

* Rakudo Star: https://rakudo.org/star

* Rakudo Star docker: https://github.com/perl6/docker

* Rakudo Star github: https://github.com/rakudo/star

* Raku modules land: https://raku.land

* Raku Language Specification: https://raku.org/specification/

* Raku Language Documentation: https://docs.raku.org/
